package ru.t1.schetinin.tm.component;

import ru.t1.schetinin.tm.api.repository.ICommandRepository;
import ru.t1.schetinin.tm.api.repository.IProjectRepository;
import ru.t1.schetinin.tm.api.repository.ITaskRepository;
import ru.t1.schetinin.tm.api.repository.IUserRepository;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.command.AbstractCommand;
import ru.t1.schetinin.tm.command.project.*;
import ru.t1.schetinin.tm.command.system.*;
import ru.t1.schetinin.tm.command.task.*;
import ru.t1.schetinin.tm.command.user.*;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.schetinin.tm.exception.system.CommandNotSupportedException;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.repository.CommandRepository;
import ru.t1.schetinin.tm.repository.ProjectRepository;
import ru.t1.schetinin.tm.repository.TaskRepository;
import ru.t1.schetinin.tm.repository.UserRepository;
import ru.t1.schetinin.tm.service.*;
import ru.t1.schetinin.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ApplicationAboutCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectListCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCompleteByIdCommand());

        registry(new TaskListCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskShowByProjectIdCommand());

        registry(new UserLoginCommand());
        registry(new UserRegistryCommand());
        registry(new UserViewProfileCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserLogoutCommand());

        registry(new ApplicationExitCommand());
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("OK");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** TASK MANAGER IS SHOOTING DOWN ***");
            }
        });
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void exit() {
        System.exit(0);
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("user", "user", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.add(new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("DEMO PROJECT", Status.NOT_STARTED));
        projectService.add(new Project("BEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("BETA PROJECT", Status.COMPLETED));

        taskService.create("MEGA TASK");
        taskService.create("BETA TASK");
        taskService.create("SUPER TASK");
        taskService.create("CLEAN TASK");
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command){
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String[] args) {
        processArguments(args);
        initDemoData();
        initLogger();
        processCommands();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}