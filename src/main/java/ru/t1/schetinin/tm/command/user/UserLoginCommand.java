package ru.t1.schetinin.tm.command.user;

import ru.t1.schetinin.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    private static final String NAME = "login";

    private static final String DESCRIPTION = "User login.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}