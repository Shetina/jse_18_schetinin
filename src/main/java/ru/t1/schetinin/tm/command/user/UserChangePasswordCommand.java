package ru.t1.schetinin.tm.command.user;

import ru.t1.schetinin.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    private static final String NAME = "change-user-password";

    private static final String DESCRIPTION = "Change password of current user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}