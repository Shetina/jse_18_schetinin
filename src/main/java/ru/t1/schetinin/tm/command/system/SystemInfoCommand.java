package ru.t1.schetinin.tm.command.system;

import static ru.t1.schetinin.tm.util.FormatByteUtil.formatByte;

public final class SystemInfoCommand extends AbstractSystemCommand{

    public static final String NAME = "info";

    public static final String DESCRIPTION = "Show system info.";

    public static final String ARGUMENT = "-i";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        System.out.println("PROCESSORS: " + processorCount);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();

        System.out.println("MAX MEMORY: " + formatByte(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatByte(totalMemory));
        System.out.println("FREE MEMORY: " + formatByte(freeMemory));
        System.out.println("USED MEMORY: " + formatByte(totalMemory - freeMemory));
    }

}