package ru.t1.schetinin.tm.command.user;

import ru.t1.schetinin.tm.api.service.IAuthService;
import ru.t1.schetinin.tm.api.service.IUserService;
import ru.t1.schetinin.tm.command.AbstractCommand;
import ru.t1.schetinin.tm.exception.user.UserNotFoundException;
import ru.t1.schetinin.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService(){
        return serviceLocator.getUserService();
    }

    public IAuthService getAuthService(){
        return serviceLocator.getAuthService();
    }

    protected void showUser(final User user){
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Override
    public String getArgument() {
        return null;
    }

}