package ru.t1.schetinin.tm.command.project;

import ru.t1.schetinin.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand{

    private static final String NAME = "project-update-by-id";

    private static final String DESCRIPTION = "Update project by id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateById(id, name, description);
    }

}
